# CloudFormation CICD

This project contains a GitLab-CI Pipeline for the delivery and deployment of AWS CloudFormation Templates that can be reused across multiple projects.

The sole purpose of this project is to be able to run an identical pipeline across all projects that deploy CloudFormation from GitLab.

Changes to this project will be tagged and versions to current use of this pipeline in remote projects will not be affected. Updates and enhancements to this pipeline will be included and tagged as a new version.

## Prerequisite(s)

Prior to using this CI Pipeline, the following configurations must be completed.

### GitLab OIDC IAM Idp/Role Pair

It is required to have a GitLab OIDC IdP and Role deployed into your AWS Account(s).

To easily complete this deployment please visit:

[https://gitlab.com/rwickit/cicd/aws-gitlab-cicd](https://gitlab.com/rwickit/cicd/aws-gitlab-cicd).

### GitLab CICD Variables

Provide GitLab Variables with the output role names from the previous step.

The CICD Variables can be placed at individual project level or any group or sub-group above the current project. However, if any lower level group variable match the variable name, they will take precedent.

Details about GitLab Variables: [https://docs.gitlab.com/ee/ci/variables/](https://docs.gitlab.com/ee/ci/variables/).

## Artifacts

The resources included in this project are:

- CI: Individual AWS Job includes
- SCA: Static Code Analysis Jobs
- Examples: .gitlab-ci.yml configurations for us in remote projects

Additional directories may be included in the future to accommodate additions or modifications to new versions of the pipeline.

## Implementation

To implement this remote pipeline into an existing project, simply replace or create the `.gitlab-ci.yml` with the following configurations.

### Static Code Analysis (SCA)

This at a minimum will run SCA on your current CloudFormation project.

```yml
include:
  - project: rwickit/cicd/cfn
    ref: v1 ## The branch, tag, or SHA to use  (default: main)
    file: review.yml ## The path to the file to include (default: main.yml)

variables:
  ## PROJECT VARIABLES
  SOURCE_DIR: ${CI_PROJECT_DIR}/cloudformation/
```

### SCA & Deploy

This will run SCA and deliver all contents of the `SOURCE_DIR` to the s# configuration in variables.

```yml
include:
  - project: rwickit/cicd/cfn
    ref: v1 ## The branch, tag, or SHA to use  (default: main)
    file: review.yml ## The path to the file to include (default: main.yml)

variables:
  ## PROJECT VARIABLES
  SOURCE_DIR: ${CI_PROJECT_DIR}/cloudformation/

  ## DELIVER VARIABLES
  BUCKET: << enter bucket name >>
  BUCKET_REGION: << bucket region >>
  DELIVERY_PATH: ${BUCKET}/${CI_PROJECT_NAME} ## change prefix path for desired location

## JOBS:

deliver:template:
  stage: deliver
  extends: .deliver
  variables:
    ROLE_ARN: ${ROLE_ARN} ## in a variable or hardcoded here, paste the ROLE ARN in Delivery Bucket account that has permissions to `s3:PutObject`
```

### Deploy CloudFormation (no params)

The following snippet will complete SCA, deliver updated templates to the configured S3 Bucket, Stage a CloudFormation ChangeSet, and Wait on manual action to Deploy the template in the account.

This configuration will deploy the template resources as is with `required` default parameters.

```yml
include:
  - project: rwickit/cicd/cfn
    ref: v1 ## The branch, tag, or SHA to use  (default: main)
    file: review.yml ## The path to the file to include (default: main.yml)

variables:
  ## PROJECT VARIABLES
  SOURCE_DIR: ${CI_PROJECT_DIR}/cloudformation/

  ## DELIVER VARIABLES
  BUCKET: << enter bucket name >>
  BUCKET_REGION: << bucket region >>
  DELIVERY_PATH: ${BUCKET}/${CI_PROJECT_NAME} ## change prefix path for desired location

  ## DEPLOY VARIABLES
  CLI_REGION: << desired region for resource(s) >>
  STACK_NAME: << desired stack name >>
  TEMPLATE: << name of template file >>.yml
  DEPLOY_PATH: ${CI_PROJECT_NAME}/${TEMPLATE}

## JOBS:

deliver:template:
  stage: deliver
  extends: .deliver
  variables:
    ROLE_ARN: ${DELIVER_ROLE_ARN} ## in a variable or hardcoded here, paste the ROLE ARN in Delivery Bucket account that has permissions to `s3:PutObject`

stage:dev:
  stage: stage
  extends: .stage
  needs: ["deliver:template"]
  variables:
    ROLE_ARN: ${DEPLOY_ROLE_ARN}

deploy:dev:
  stage: deploy
  extends: .deploy
  needs: ["stage:dev"]
  variables:
    ROLE_ARN: ${DEPLOY_ROLE_ARN}
```

### Deploy CloudFormation (with params)

The following snippet will complete SCA, deliver updated templates to the configured S3 Bucket, Stage a CloudFormation ChangeSet, and Wait on manual action to Deploy the template in the account.

This configuration will read in a param file per account and deploy with unique variables.

**Steps for required parameters**

To deploy your templates with unique parameters your project must have a `params` directory in the root directory.

Within the params directory, create a `{identifier}.json` file with standard CloudFormation Parameters.

Use the filename (without file extension) as a parameter in your `stage` and `deploy` jobs.

> Example: `prod.json`

```yml
include:
  - project: rwickit/cicd/cfn
    ref: v1 ## The branch, tag, or SHA to use  (default: main)
    file: review.yml ## The path to the file to include (default: main.yml)

variables:
  ## PROJECT VARIABLES
  SOURCE_DIR: ${CI_PROJECT_DIR}/cloudformation/

  ## DELIVER VARIABLES
  BUCKET: << enter bucket name >>
  BUCKET_REGION: << bucket region >>
  DELIVERY_PATH: ${BUCKET}/${CI_PROJECT_NAME} ## change prefix path for desired location

  ## DEPLOY VARIABLES
  CLI_REGION: << desired region for resource(s) >>
  STACK_NAME: << desired stack name >>
  TEMPLATE: << name of template file >>.yml
  DEPLOY_PATH: ${CI_PROJECT_NAME}/${TEMPLATE}

## JOBS:

deliver:template:
  stage: deliver
  extends: .deliver
  variables:
    ROLE_ARN: ${DELIVER_ROLE_ARN} ## in a variable or hardcoded here, paste the ROLE ARN in Delivery Bucket account that has permissions to `s3:PutObject`

stage:dev:
  stage: stage
  extends: .stage
  needs: ["deliver:template"]
  variables:
    PARAMS: dev
    ROLE_ARN: ${DEPLOY_ROLE_ARN}

deploy:dev:
  stage: deploy
  extends: .deploy
  needs: ["stage:dev"]
  variables:
    PARAMS: dev
    ROLE_ARN: ${DEPLOY_ROLE_ARN}
```

### Multiple Account Deployment

To deploy the same resources with or without specific parameters to additional accounts, all you need to do is add an additional `stage` and `deploy` extends jobs to your CI and if using parameters, update the `param` variable.

```yml
stage:prod:
  stage: stage
  extends: .stage
  needs: ["deliver:template"]
  variables:
    PARAMS: prod
    ROLE_ARN: ${DEPLOY_ROLE_ARN}

deploy:prod:
  stage: deploy
  extends: .deploy
  needs: ["stage:dev"]
  variables:
    PARAMS: prod
    ROLE_ARN: ${DEPLOY_ROLE_ARN}
```

## Example CI Templates

View Example `.gitlab-ci.yml` templates in the [examples](examples/) directory.

To review and example project with a completed and successful `deploy` pipeline, please review [https://gitlab.com/rwickit/demos/ci-and-cd-on-amazon-web-service-aws/gitlab-aws-cloudformation](https://gitlab.com/rwickit/demos/ci-and-cd-on-amazon-web-service-aws/gitlab-aws-cloudformation).
